package com.ils.challenge.jobs;

import com.ils.challenge.model.Squirrel;
import com.ils.challenge.processor.SquirrelItemProcessor;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.job.builder.JobBuilder;
import org.springframework.batch.core.repository.JobRepository;
import org.springframework.batch.core.step.builder.StepBuilder;
import org.springframework.batch.item.ItemProcessor;
import org.springframework.batch.item.ItemReader;
import org.springframework.batch.item.ItemWriter;
import org.springframework.batch.item.file.FlatFileItemReader;
import org.springframework.batch.item.file.mapping.BeanWrapperFieldSetMapper;
import org.springframework.batch.item.file.mapping.DefaultLineMapper;
import org.springframework.batch.item.file.transform.DelimitedLineTokenizer;
import org.springframework.batch.item.json.JacksonJsonObjectMarshaller;
import org.springframework.batch.item.json.JsonFileItemWriter;
import org.springframework.batch.item.json.builder.JsonFileItemWriterBuilder;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.FileSystemResource;
import org.springframework.transaction.PlatformTransactionManager;

@Configuration
public class CvsToJson {

    private final static String[] FIELDS = {
            "x", "y", "unique_squirrel_id", "hectare", "shift", "date", "hectare_squirrel_number", "age",
            "primary_fur_color", "highlight_fur_color", "color_combination", "color_notes", "location", "measurement",
            "specific_location", "running", "chasing", "climbing", "eating", "foraging", "other_activities", "kuks",
            "quaas", "moans", "tail_flags", "tail_twitches", "approaches", "indifferent", "runs_from",
            "other_interactions", "lat_long"
    };

    private final JobRepository jobRepository;
    @Value("${input.file}")
    private String inputUrl;
    @Value("${output.file}")
    private String outputUrl;

    public CvsToJson(JobRepository jobRepository) {
        this.jobRepository = jobRepository;
    }

    @Bean(name = "csvToJsonJob")
    public Job job(Step step) {
        var name = "Job #1 ";
        return new JobBuilder(name, jobRepository)
                .start(step)
                .build();
    }

    @Bean
    public Step step(ItemReader<Squirrel> reader,
                     ItemWriter<Squirrel> writer,
                     ItemProcessor<Squirrel, Squirrel> processor,
                     PlatformTransactionManager tM) {
        var name = "Step #1";
        return new StepBuilder(name, jobRepository)
                .<Squirrel, Squirrel>chunk(5, tM)
                .reader(reader)
                .processor(processor)
                .writer(writer)
                .build();
    }

    @Bean
    public FlatFileItemReader<Squirrel> reader() {
        var itemReader = new FlatFileItemReader<Squirrel>();
        itemReader.setLineMapper(new DefaultLineMapper<>() {{
            setLineTokenizer(new DelimitedLineTokenizer() {{
                setDelimiter(",");
                setNames(FIELDS);
            }});
            setFieldSetMapper(new BeanWrapperFieldSetMapper<>() {{
                setTargetType(Squirrel.class);
            }});
        }});
        itemReader.setResource(new ClassPathResource(inputUrl));
        return itemReader;
    }

    @Bean
    public JsonFileItemWriter<Squirrel> writer() {
        return new JsonFileItemWriterBuilder<Squirrel>()
                .name("writer")
                .jsonObjectMarshaller(new JacksonJsonObjectMarshaller<>())
                .resource(new FileSystemResource(outputUrl))
                .build();
    }

    @Bean
    public SquirrelItemProcessor processor() {
        return new SquirrelItemProcessor();
    }
}
